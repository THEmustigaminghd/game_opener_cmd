# game_opener_cmd

This program allows you to launch a game, launcher and emulator on cmd (Windows Command prompt). This is a good way to access all your games just by writing a simple command.

## PC requirements
Any version of Python installed on Command Prompt

Reccomended: Python 3.11.3

Note: In order to obtain Python on your PC command prompt you must go to Microsoft Store on your Windows Start menu. Once you do that, you download Python (ideally 3.10 or 3.11). 

## How to download

- Step 1 Click on the download icon which is beside the Clone button.

- Step 2 Once you click the download button, click which source code you want to download with
    ("zip" is advised).

- Step 3 After you click the source code (preferably zip file), there will be in a window where you want to place the program.
    It is advised to keep it as the default name as game_opener_cmd-main in the Downloads folder.

- Step 4 Once its downloaded, right_click on the game_opener_cmd and click "Extract here" or "Unzip here" to extract the folder.

- Step 5 All you need to do is go in the game_opener_cmd-main folder. After that, copy the "game" file to C:\Users\(username)
        In other words, after you copy the "game" file, you go to "My PC" > "Local Disk" > "Users" > (The username you are using as).
        Once you go there, paste it there.

- Step 6 Copy-paste any shortcuts of your games in the game_opener_cmd > games > Game_shortcuts folder.
    Any shortcuts of an emulator or a games launcher (i.e steam), copy-paste it in game_opener_cmd > games > launchers_and_emulators folder.

- Step 7 Go to Command Prompt (Go to Windows Start button and type "cmd").

- Step 8 write "game" on the command line on cmd.

It should say 

```
type 1 for opening games
type 2 for opening emulators and launchers
type the number here:

```

This allows you if you want to launch a game or an emulator or launcher,

```
-----------------------------------
List of games type number of desired game
-----------------------------------
0 -> <<CANCEL>>
1 -> <(The game shortcuts you put in the "Game_shortcuts" or "launchers_and_emulators" folder)>
-----------------------------------
Type the number of the game you want to play:

```

Once you type the number of the game you want to play it should launch.

This program is made by Mustafa Kamal (THEmustigaminghd).