#!/usr/bin/env python3

import subprocess
from open_game import ui_command, excecute_game

def gamelist_write():
	a = input("type 1 for opening games \ntype 2 for opening emulators and launchers \ntype the number here: ")

	if a != '1' and a != '2':
		print(f'{a} is not valid you have to type again with a valid option')

	elif a == '1':
		with open('game_list.txt', 'w') as file:
			output = subprocess.check_output('dir/s ..\games\Game_shortcuts', shell=True)
			line = output.decode('utf-8')
			file.write(line)
		gamelist_read()

	elif a == '2':
		with open('game_list.txt', 'w') as file:
			output = subprocess.check_output('dir/s ..\games\launchers_and_emulators', shell=True)
			line = output.decode('utf-8')
			file.write(line)
		gamelist_read()

def gamelist_read():
	with open('game_list.txt', 'r') as file:
		c = file.readlines()
	ui_text(c)

def ui_text(c):
	print("-----------------------------------")
	print("List of games type number of desired game")
	print("-----------------------------------")
	print("0 -> <<CANCEL>>")
	i = 3
	n = 0
	keys = []
	values = []
	while i < len(c):
		t = c[i].strip().split()[3:]
		if t != [] and t != ['.'] and t != ['..'] and 'bytes' not in t:
			n = n + 1
			g = ' '.join(t)
			keys.append(n)
			values.append(g)
			print(f'{n} -> {g}')
		i = i + 1
	ui_command(keys, values)

def main():
	gamelist_write()

if __name__ == '__main__':
	main()
