#!/usr/bin/env python3

import subprocess

def ui_command(keys, values):
	print("-----------------------------------")
	my_dict = {key: value for key, value in zip(keys, values)}
	code = int(input('Type the number of the game you want to play: '))
	num = list(my_dict.keys())
	game = list(my_dict.values())
	if code == 0:
		print("Cancelled")
	elif code in num:
		chosen_game = game[code-1]
		print(chosen_game)
		excecute_game(chosen_game)
	else:
		print("Invalid Input")

def excecute_game(c):
	gn = " ".join(c.split('.')[:-1])
	print(f'Opening {gn} ...')
	with open('game_list.txt', 'r') as file:
		a = file.readlines()
	i = 0
	while i < len(a):
		t = a[i].strip().split()
		if 'Directory' in t:
			d = t[-1]
			dir = d + "\\" + '"' + c + '"'
			open_game(dir)
		i = i + 1

def open_game(x):
	command = 'start ' + x
	output = subprocess.check_output(command, shell=True)
	line = output.decode('utf-8')
